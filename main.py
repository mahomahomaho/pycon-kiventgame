# pylint: disable=unused-import
from math import radians
from random import randint

from cymunk import Vec2d
from kivy.app import App
from kivy.base import EventLoop
from kivy.core.window import Keyboard, Window
from kivy.uix.widget import Widget
from kivy.vector import Vector

import kivent_core  # noqa: F401
from kivent_core.managers.resource_managers import texture_manager
import kivent_cymunk  # noqa: F401


import defs

texture_manager.load_image('img/fred.png')
texture_manager.load_image('img/baloon.png')


class KiventGame(Widget):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.id_fred = None
        self.ids_baloons = []
        self.id_walls = None
        self.gameworld.init_gameworld(['cymunk_physics', 'rotate', 'rotate_renderer', 'position'],
                                      callback=self.init_game)
        EventLoop.window.bind(on_key_up=self.on_key_up)
        Window.bind(on_resize=self.on_resize)

    def init_game(self):
        self.setup_states()
        self.set_state()
        self.draw_objects()
        self.create_walls()

        self.physics.space.add_collision_handler(defs.goal_collision_type,
                                                 defs.fred_collision_type,
                                                 self.goal_reached)

    def setup_states(self):
        self.gameworld.add_state(state_name='main',
                                 systems_added=[],
                                 systems_removed=[], systems_paused=[],
                                 systems_unpaused=[],
                                 screenmanager_screen='main')
        self.gameworld.add_state(state_name='success',
                                 systems_added=[],
                                 systems_removed=[], systems_paused=['cymunk_physics'],
                                 systems_unpaused=[],
                                 screenmanager_screen='success')

    def set_state(self):
        self.gameworld.state = 'main'

    def draw_object(self, texture, pos, size, collision_type=defs.default_collision_type):
        w, h = size
        R = (w + h) / 4
        create_dict = {
            'cymunk_physics': {
                'main_shape': 'circle',
                'velocity': (0, 0),
                'position': pos,
                'vel_limit': 1250,
                'mass': defs.mass,
                'col_shapes': [{
                    'shape_type': 'circle',
                    'elasticity': defs.elasticity,
                    'collision_type': collision_type,
                    'friction': defs.friction,
                    'shape_info': {
                        'inner_radius': 0, 'outer_radius': R,
                        'mass': defs.mass, 'offset': (0, 0)
                    }
                }],
                'angular_velocity': 0,  # literalnie wszystkie klucze muszą być wypełnione
                'ang_vel_limit': 0,
                'angle': radians(180)
            },
            'rotate_renderer': {
                'texture': texture,
                'size': size,
                'render': True
            },
            'position': pos,
            'rotate': 0
        }
        # id fred, to jest int - identyfikator Freda w systemach (Position, Renderer, Cymunk)
        return self.gameworld.init_entity(create_dict,
                                                  ['position', 'rotate',
                                                   'rotate_renderer',
                                                   'cymunk_physics'])

    def draw_objects(self):
        self.id_fred = self.draw_object('fred', (300, 300), (145, 145), defs.fred_collision_type)
        for __ in range(defs.num_baloons):
            self.ids_baloons.append(self.draw_object('baloon',
                                                     (randint(100, 800), randint(100, 600)),
                                                     (15, 15)))

    def create_walls(self):
        segments = self.wall_segments()

        shapes = []

        for (v1, v2), goal in zip(segments, [False, False, False, True]):
            coltype = defs.goal_collision_type if goal else defs.default_collision_type
            shapes.append({'shape_type': 'segment',
                           'elasticity': defs.elasticity,
                           'collision_type': coltype,
                                'friction': defs.friction,
                                'shape_info': {'a': v1, 'b': v2, 'radius': defs.wall_size,
                                               'mass': 0}  # mass: 0 => obiekt statyczny
                           })
        self.id_walls = self.gameworld.init_entity({
                        'cymunk_physics': {
                            'main_shape': 'shape',
                            'velocity': (0, 0),
                            'position': (0, 0),
                            'vel_limit': 0,
                            'mass': 0,
                            'col_shapes': shapes,
                            'angular_velocity': 0,
                            'ang_vel_limit': 0,
                            'angle': 0
                        },
                        'position': (0, 0),
                        'rotate': 0
                  }, ['position', 'rotate', 'cymunk_physics'])

    def on_key_up(self, __window, key, *__, **___):
        dx, dy = 0, 0
        if key == Keyboard.keycodes['up']:
            dy = 1500
        elif key == Keyboard.keycodes['down']:
            dy = -1500
        elif key == Keyboard.keycodes['left']:
            dx = -1500
        elif key == Keyboard.keycodes['right']:
            dx = +1500

        fred = self.gameworld.entities[self.id_fred]
        fred.cymunk_physics.body.apply_impulse(Vec2d(dx, dy))

    def on_touch_up(self, touch):
        fred = self.gameworld.entities[self.id_fred]
        vdir = Vector(touch.pos) - fred.position.pos
        fred.cymunk_physics.body.apply_impulse(vdir * 5)

    def on_resize(self, _win, _w, _h):
        if self.id_walls is None:
            return
        walls = self.gameworld.entities[self.id_walls]
        for (v1, v2), shape in zip(self.wall_segments(), walls.cymunk_physics.shapes):
            shape.a = v1
            shape.b = v2
        self.physics.space.reindex_static()

    def goal_reached(self, _space, _arbiter):
        self.gameworld.state = 'success'

    def wall_segments(self):
        w, h = self.size
        R = defs.wall_size
        return [(Vec2d(-2 * R, -R), Vec2d(w + 2 * R, -R)),
                (Vec2d(-R, -2 * R), Vec2d(-R, h + 2 * R)),
                (Vec2d(-2 * R, h + R), Vec2d(w + 2 * R, h + R)),
                (Vec2d(w + R, h + 2 * R), Vec2d(w + R, -2 * R))]


class KiventGameApp(App):
    def build(self):
        pass


if __name__ == '__main__':
    KiventGameApp().run()
